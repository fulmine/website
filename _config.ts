import lume from "lume/mod.ts";
import date from "lume/plugins/date.ts";
import feed from "lume/plugins/feed.ts";
import minify_html from "lume/plugins/minify_html.ts";
import prism from "lume/plugins/prism.ts";
import reading_info from "lume/plugins/reading_info.ts";
import relative_urls from "lume/plugins/relative_urls.ts";
import resolve_urls from "lume/plugins/resolve_urls.ts";
import sitemap from "lume/plugins/sitemap.ts";
import slugify_urls from "lume/plugins/slugify_urls.ts";
import nunjucks from "lume/plugins/nunjucks.ts";

const site = lume({
  src: "/src",
  server: {
    page404: "/not-found/index.html"
}
});

site.copy("/static")
site.loadAssets([".css", ".js"])

site.use(nunjucks())
site.use(date());
site.use(feed());
site.use(minify_html());
site.use(prism());
site.use(reading_info());
site.use(relative_urls());
site.use(resolve_urls());
site.use(sitemap());
site.use(slugify_urls());

export default site;
