---
title: Welcome!
---

## Hi there!
**I'm Jake** (he/they), I go by as Fulmine, Fxlmine or Draconizations online. I draw art and write code and the like!

This website is [a work in progress](https://codeberg.org/Fulmine/website), there's still a lot to be done here. But I wanted to get the base template up and available as soon as I could.

<hr>

### Here's some notable projects
- [PluralKit's dashboard](https://github.com/PluralKit/PluralKit/tree/main/dashboard): the official dashboard for the discord bot PluralKit
- [Spectral](https://modrinth.com/resourcepack/spectral): A 16x Minecraft (Java Edition) Resource Pack, entirely with pixel art done by me
- [urlcutter](https://github.com/Draconizations/urlcutter): a personal, one-user URL shortener service written in SvelteKit
- [Pluralkit.xyz](https://codeberg.org/fulmine/pk-fulmine): A website written in SvelteKit that displays public system information from PluralKit

### And here's some links
- [Github](https://github.com/Draconizations)
- [Codeberg](https://codeberg.org/Fulmine)
- [Tumblr](https://fxlmine.tumblr.com)
- [Twitter](https://twitter.com/fxlmine) (pretty much dead)
- [Fedi/Mastodon](https://tech.lgbt/@fulmine)
- [Cohost](https://cohost.org/fulmine)

<hr>

That's about it for now, I think! And I [do have a ko-fi](https://ko-fi.com/fxlmine) in case someone's feeling generous.